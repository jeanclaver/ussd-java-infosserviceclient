/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClient.data;
import dbaccess.dbsrver.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import java.util.*;
/**
 *
 * @author mpdiallo
 */
public class DataINFOSERVICE extends AbstractDBSrverClient {
    public int insertTransaction (String transactionId, String msisdn, String used_option) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append(" INSERT INTO  ");
            sb.append(" INFOS_SERVICE_CLIENT_TRANSAC ( ");
            sb.append(" TRANSACTION_ID, MSISDN, OPTION_USED)");
            sb.append(" VALUES (   ?,   ?,   ?) ");
            
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString (1, transactionId);
            pstmt.setString(2, msisdn);
            pstmt.setString(3, used_option);
            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
    public int insertTransacCarteMalGratter (String transactionId, String msisdn, String customerMsisdn, String serialKey, int status) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append(" INSERT INTO  ");
            sb.append(" CARTE_MAL_GRATTEE_TRANSAC ( ");
            sb.append(" TRANSACTION_ID, MSISDN, CUSTOMER_MSISDN, SERIAL_KEY, STATUS ) ");
            sb.append(" VALUES (   ?,   ?,   ?,  ?, ?) ");
            
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString (1, transactionId);
            pstmt.setString(2, msisdn);
            pstmt.setString(3, customerMsisdn);
            pstmt.setString(4, serialKey);
            pstmt.setInt(5, status);
            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
    public int insertTransacPAYG(String transactionId, String msisdn, String statusPAYG){
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append(" INSERT INTO  ");
            sb.append(" APP_USER.PAYG_STATUS_TRANSAC ( ");
            sb.append(" TRANSACTION_ID, MSISDN, STATUS ) ");
            sb.append(" VALUES (   ?,   ?,   ?) ");
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString (1, transactionId);
            pstmt.setString(2, msisdn);
            pstmt.setString(3, statusPAYG);
            pstmt .executeUpdate();
           
        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
    public InfoServiceClient getSerialKey(String serialKey) {
        InfoServiceClient resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from CARTE_MAL_GRATTEE_TRANSAC where SERIAL_KEY = '"+serialKey+"' ");
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                String  serialNumber = rs.getString("SERIAL_KEY");

                resp = new InfoServiceClient();
                resp.setSerialKey(serialNumber);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
}