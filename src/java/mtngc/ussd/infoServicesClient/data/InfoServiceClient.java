/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClient.data;

import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class InfoServiceClient implements Serializable {
    
    private String statut;
    private String serialKey;

    /**
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * @param statut the statut to set
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * @return the serialKey
     */
    public String getSerialKey() {
        return serialKey;
    }

    /**
     * @param serialKey the serialKey to set
     */
    public void setSerialKey(String serialKey) {
        this.serialKey = serialKey;
    }
    
}
