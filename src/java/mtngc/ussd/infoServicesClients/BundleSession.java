/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClients;
import java.io.Serializable;
import mtngc.ussd.*;
/**
 *
 * @author EKhosa
 */
public class BundleSession  {
    private String ussdSessionId;
    private RequestEnum selection;
    private int step;
    private String customerMSISDN;
    private String customerMSISDNConf;
    private String serialID;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the selection
     */
    public RequestEnum getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(RequestEnum selection) {
        this.selection = selection;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the customerMSISDN
     */
    public String getCustomerMSISDN() {
        return customerMSISDN;
    }

    /**
     * @param customerMSISDN the customerMSISDN to set
     */
    public void setCustomerMSISDN(String customerMSISDN) {
        this.customerMSISDN = customerMSISDN;
    }

    /**
     * @return the customerMSISDNConf
     */
    public String getCustomerMSISDNConf() {
        return customerMSISDNConf;
    }

    /**
     * @param customerMSISDNConf the customerMSISDNConf to set
     */
    public void setCustomerMSISDNConf(String customerMSISDNConf) {
        this.customerMSISDNConf = customerMSISDNConf;
    }

    /**
     * @return the serialID
     */
    public String getSerialID() {
        return serialID;
    }

    /**
     * @param serialID the serialID to set
     */
    public void setSerialID(String serialID) {
        this.serialID = serialID;
    }

   
}
