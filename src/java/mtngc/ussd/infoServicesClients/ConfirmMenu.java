/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClients;

import java.util.Enumeration;

/**
 *
 * @author mpdiallo
 */
public class ConfirmMenu {
    private RequestEnum requestEnum;
    
    public ConfirmMenu(RequestEnum requestEnum){
      this.requestEnum=requestEnum;  
    }
    public String getString(){
        StringBuilder sb = new StringBuilder();
        if(requestEnum == RequestEnum.CODE_SERVICE){
            sb.append("*223# Consulter son solde\n");
            sb.append("*100*1# Forfait MTN ComJaim\n");
            sb.append("*100*3# Forfait Internet\n");
            sb.append("*223*20# Consulter Solde internet\n");
            sb.append("*440# Service Mobile Money\n");
            sb.append("*112# Verifier son enregistrement\n");
            sb.append("*102# Partager des credits\n");
            sb.append("*200# Emprunter des credits,comjaim ou internet\n");
            sb.append("*444# Services a valeur ajoutee\n");
            sb.append("*411# Desactiver Service a Valeur Ajoutee\n");
            sb.append("*224*Numero Carte#: Pour Recharger\n");
            sb.append("*311# Historique des transactions\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.SERVICE_DIGITAL){
            sb.append("WhatsApp: 660222222\n");
            sb.append("Facebook: MTN Guinee\n");
            sb.append("YouTube: MTN Guinee\n");
            sb.append("LinkedIn: MTN Guinee\n");
            sb.append("Twitter: @MTNGuinee\n");
            sb.append("E-mail: serviceclient.GN@mtn.com\n");
            sb.append("Tapez 1 Pour une demande d assistance\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.CENTRE_APPEL){
            sb.append("8800: Centre d appel prepaye\n");           
            sb.append("8555: Centre d appel pour Distributeur\n");
            sb.append("8777: Centre d appel post paye\n");
            //sb.append("8888: Centre d appel Mobile Money\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.AGENCE){
            sb.append("1. Conakry\n");           
            sb.append("2. Interieur du pays\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.YELLOW_BOUTIQUE){
            sb.append("1. Conakry\n");           
            sb.append("2. Basse Guinee\n");
            sb.append("3. Moyenne Guinee\n");
            sb.append("4. Haute Guinee\n");
            sb.append("5. Guinee Forestiere\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.PAYG){
            sb.append("Facturation hors forfaits\n");
            sb.append("1. Activer\n");           
            sb.append("2. Desactiver\n");
            sb.append("# Retour\n");
            sb.append("Repondez");
        }else if(requestEnum == RequestEnum.CONFIG_MOBILE){
            sb.append("Saisissez votre numero de telephone\n");           
            sb.append("# Retour\n");
            sb.append("Repondez");
        }
        String str = sb.toString();
                
        return str;
    }
    
}
