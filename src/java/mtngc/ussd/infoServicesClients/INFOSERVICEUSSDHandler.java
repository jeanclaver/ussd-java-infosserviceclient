/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClients;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import mtngc.ussd.infoServicesClient.data.DataINFOSERVICE;
import mtngc.ussd.infoServicesClient.data.InfoServiceClient;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

import org.apache.commons.lang.StringUtils;
import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.UCIPUpdateBalanceAndDateResponse;
import ucipclient.UCIPUpdateServiceClassResponse;
import ucipclient.UCIPUpdateRefillIDResponse;
/**
 *
 * @author mpdiallo
 */
public class INFOSERVICEUSSDHandler  {
    
    static String ContextKeyName;
    static String PAYG_Serive = "PAYG";
    static ArrayList <Integer> supportedMSISDNs;
    String FreeFlow;
    
    static
    {  
        ContextKeyName = "InfosServcicClt";
        String filePath = "C:\\Logs\\"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
    }
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
        TraceParametersAndHeaders(request);
            
        try {
            String msisdnStr = request.getParameter("MSISDN");
            //msisdnStr = "224660134178";
            int msisdn = 0;
            try{msisdnStr = msisdnStr.substring(3);}catch(Exception n){};
            try{msisdn = Integer.parseInt(msisdnStr);}catch(Exception n){};
            
            if(IsMSISDNAllowed(msisdn)){
                processRequest(msisdnStr,request, response);
            }else {
                response.setContentType("UTF-8");
                response.setHeader("Freeflow", "FB");
                response.setHeader("cpRefId", "emk2545");
                PrintWriter out = response.getWriter();
                out.append("Le service n'est pas disponible");           
                out.close();            
            }
            
        } catch (Exception e) {
            MLogger.Log(this, e);
        }    
    }
    
    public void processRequest(String msisdn,HttpServletRequest request, HttpServletResponse response){
        this.FreeFlow = "FC";
        try {
            
            String ussdSessionid = request.getParameter("SESSIONID");            
            String input = request.getParameter("INPUT");
            
            //String msg = null;
   
            ServletContext context = request.getServletContext();
            
            if(input !=null){
                if(input.endsWith("*2")){
                   processShortCut(msisdn, ussdSessionid,RequestEnum.CODE_SERVICE, context);                    
                   input = "2";
                   MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input "+input);
                }else if(input.endsWith("*3")){
                    processShortCut(msisdn, ussdSessionid,RequestEnum.SERVICE_DIGITAL, context);
                    input = "3";
                    MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input "+input);
                }
            }
            
                StringBuilder sb = new StringBuilder();
             
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"| Processing request.");
                            
                String contextKey = GetContextKey(ussdSessionid);
                Object obj = context.getAttribute(contextKey);
                if(obj == null){
                    if(input != null){
                        String str = displayMainMenu(context, ussdSessionid, msisdn);
                        sb.append(str );
                    }else
                        sb.append("Application is running");
                }else {
                    
                    BundleSession bundleSession = (BundleSession) obj;
                    int step = bundleSession.getStep();
                switch (step) {
                    case 1:
                        {
                            // if previous screen was the main menu
                            String str = processMainMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str);
                            break;
                        }
                    case 2:
                        {
                            String str = processConfirmMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str);
                            break;
                        }
                    case 3:
                        {
                            String str = processConfirmMenu(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str);
                            break;
                        }
                    default:
                        {
                            // if previous step is not supported
                            String str = displayMainMenu(context, ussdSessionid, msisdn);
                            sb.append(str);
                            break;
                        }
                }
                }
                
            //}
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", FreeFlow);
            response.setHeader("cpRefId", "emk2545");
            //response.setContentType(type);
            PrintWriter out = response.getWriter();
//            out.append("Your string goes here\n");
//            out.append("Your string goes here");
            
            out.append(sb.toString());
            out.close();
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
    }
    protected String displayMainMenu(ServletContext context, String ussdSessionid, String msisdn){
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list");
        BundleSession bundleSession = new BundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        
        bundleSession.setStep(1);

        context.setAttribute(contextKey, bundleSession);
        MainMenu menu = new MainMenu();
        String str = menu.getString();
        
        return str;
    }
    public String processMainMenuInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        RequestEnum reqEnum = null;
        
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim())) ){
            DataINFOSERVICE dbInfosService = new DataINFOSERVICE();          
            input =  input.trim();
            
            //int packageTypeIndex = Integer.parseInt(input);
            if(input.equals("1")){
                reqEnum = RequestEnum.CODE_SERVICE;
                dbInfosService.insertTransaction(ussdSessionid, msisdn, reqEnum.toString());
            }else if(input.equals("2")){
                reqEnum = RequestEnum.SERVICE_DIGITAL;
                dbInfosService.insertTransaction(ussdSessionid, msisdn, reqEnum.toString());
            }else if(input.equals("3")){
                reqEnum = RequestEnum.CENTRE_APPEL;
                dbInfosService.insertTransaction(ussdSessionid, msisdn, reqEnum.toString());
            }else if(input.equals("4")){
                reqEnum = RequestEnum.AGENCE;
                dbInfosService.insertTransaction(ussdSessionid, msisdn, reqEnum.toString());
            }else if(input.equals("5")){
                reqEnum = RequestEnum.YELLOW_BOUTIQUE;
                dbInfosService.insertTransaction(ussdSessionid, msisdn, reqEnum.toString());
            }else if(input.equals("6")){
                reqEnum = RequestEnum.PAYG;
            }else if(input.equals("7")){
                reqEnum = RequestEnum.CONFIG_MOBILE;
            }
            ConfirmMenu confirmMenu = new ConfirmMenu(reqEnum);            
            msg = confirmMenu.getString();
            bundleSession.setStep(2);
            bundleSession.setSelection(reqEnum);
            String contextKey = GetContextKey(ussdSessionid);
            context.setAttribute(contextKey, bundleSession); 
                                             
        }
        else{
            //naughty user, he entered wrong information
            msg = displayMainMenu(context, ussdSessionid, msisdn);
        }    
        return msg;
    }
        
    public String processConfirmMenuInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        StringBuilder sb = new StringBuilder();
        ProvisioningHorsForfaitsEngine provengin = new ProvisioningHorsForfaitsEngine();
        MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input "+input);
        DataINFOSERVICE dbInfosService = new DataINFOSERVICE();
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() >= 1) && (StringUtils.isNumeric(input.trim())) ){
           
            input =  input.trim();
            bundleSession.setCustomerMSISDN(input);
            
            //bundleSession.setCustomerMSISDNConf(input);
            
            String customerMSISDN = bundleSession.getCustomerMSISDN();
            //String customerMsisdnConf = bundleSession.getCustomerMSISDNConf();
            RequestEnum reqEnum = bundleSession.getSelection();
            MLogger.Log(this, LogLevel.DEBUG, "RequestEnum "+reqEnum.toString());
            
            if(input.equals("1") && reqEnum == RequestEnum.AGENCE){
                sb.append("Aeroport.\n");
                sb.append("Almamya Siege MTN.\n");
                sb.append("Coleah Station Shell.\n");
                sb.append("# Retour\n");
                msg =sb.toString();
            }else if(input.equals("2") && reqEnum == RequestEnum.AGENCE){
                sb.append("Kamsar Stade de l amitie.\n");
                sb.append("Labe Quartier Mairie.\n");
                sb.append("Kankan Rond Point Mbalia.\n");
                sb.append("NZerekore Yankadissa.\n");
                sb.append("# Retour.\n");
                msg =sb.toString();
            }else if(input.equals("1") && reqEnum == RequestEnum.YELLOW_BOUTIQUE){
                sb.append("Carrefour CBG.\n");
                sb.append("Lambangni Carrefour-Dadis.\n");
                sb.append("Carrefour Cimenterie.\n");
                sb.append("Matam Sur la route du niger en face de la bicigui.\n");
                sb.append("A enco 5 a cote de la station BTN.\n");
                sb.append("Madina Corniche en face du cimetiere.\n");
                sb.append("Kipe Prima Center.\n");
                sb.append("KALOUM 6eme Avenue.\n");
                sb.append("MATOTO Khabitayah en face de la radio Espace.\n");
                sb.append("# Retour.\n");
                msg =sb.toString();
            }else if(input.equals("2") && reqEnum == RequestEnum.YELLOW_BOUTIQUE){
                sb.append("Au km 5.\n");
                sb.append("Au carrefour Kagbelen à cote de NSIA Banque.\n");
                sb.append("Coyah Gare Routiere\n");
                sb.append("FORECARIAH Kassagui\n");
                sb.append("Kindia Manquepas\n");
                sb.append("Telemele Mendia.\n");
                sb.append("En face de l usine Topaz coyah\n");
                sb.append("FRIA Alpha Yaya.\n");
                sb.append("BOKE Dembaya.\n");
                sb.append("# Retour.\n");
                msg =sb.toString();
            }else if(input.equals("3") && reqEnum == RequestEnum.YELLOW_BOUTIQUE){
                sb.append("LABE Corolla.\n");
                sb.append("Rue Commercial de Mamou.\n");
                sb.append("Mamou Ecobank.\n");
                sb.append("# Retour.\n");
                msg =sb.toString();
            }else if(input.equals("4") && reqEnum == RequestEnum.YELLOW_BOUTIQUE){
                sb.append("Siguiri ORES.\n");
                sb.append("Faranah sur la nationale a Kamandiabiya\n");
                sb.append("Kouroussa Wassakoh a cote de la grande mosquee\n");
                sb.append("# Retour.\n");
                msg =sb.toString();
            }else if(input.equals("5") && reqEnum == RequestEnum.YELLOW_BOUTIQUE){
                sb.append("Dieke a cote de ancienne Ecobank.\n");
                sb.append("Macenta dans la cours du super marche boutique No16 en face de la maison des jeunes.\n");
                sb.append("Kissidougou quartie Timbo pres de VISAT BANK.\n");
                sb.append("NZerekore Diankadissa.\n");
                sb.append("# Retour.\n");
                msg =sb.toString();
            }else if(input.equals("1") && reqEnum == RequestEnum.PAYG){
                ResponseEnum respEnum = provengin.execute(msisdn, ussdSessionid, RequestEnum.ACTIVATE, PAYG_Serive);
                if(respEnum == ResponseEnum.SUCCESS){
                    sb.append("Felicitation vous avez active avec succes la facturation hors forfaits.\n");
                    msg =sb.toString();
                    dbInfosService.insertTransacPAYG(ussdSessionid, msisdn, "ACTIVATE");
                }else if(respEnum == ResponseEnum.ALREADEXIST){
                   sb.append("Vous avez deja active la facturation hors forfaits.\n");
                   msg =sb.toString();
                }else{
                    sb.append("Erreur.\n");
                    msg =sb.toString();
                }
            }else if(input.equals("2") && reqEnum == RequestEnum.PAYG){
                ResponseEnum respEnum = provengin.execute(msisdn, ussdSessionid, RequestEnum.DEACTIVATE, PAYG_Serive);
                if(respEnum == ResponseEnum.SUCCESS){
                    sb.append("Felicitation vous avez desactive avec succes la facturation hors forfaits.\n");
                    msg =sb.toString();
                    dbInfosService.insertTransacPAYG(ussdSessionid, msisdn, "DEACTIVATE");
                }else{
                   sb.append("Erreur.\n");
                    msg =sb.toString();
                }
            }else if(input.equals(customerMSISDN) && reqEnum == RequestEnum.CONFIG_MOBILE){
                reqEnum = RequestEnum.MOBILE;
                //DataINFOSERVICE dbInfosService = new DataINFOSERVICE();
                dbInfosService.insertTransacCarteMalGratter(ussdSessionid, msisdn, customerMSISDN, "CONFIG_MOBILE", 0);
            }else if(input.equals("1") && reqEnum == RequestEnum.SERVICE_DIGITAL){
                reqEnum = RequestEnum.ASSISTANCE;
            }else{
                msg = displayMainMenu(context, ussdSessionid, msisdn);
            }
            if(!input.equals("1") && !input.equals("2") && !input.equals("3") && !input.equals("4") && !input.equals("5")){
                ConfirmMenuFinal confirmMenuFinal = new ConfirmMenuFinal(reqEnum);            
                msg = confirmMenuFinal.getString();
                bundleSession.setStep(3);
                bundleSession.setSelection(reqEnum);
                bundleSession.setCustomerMSISDN(customerMSISDN);
                String contextKey = GetContextKey(ussdSessionid);
                context.setAttribute(contextKey, bundleSession);
            }
                                
        }else{
            //naughty user, he entered wrong information
            msg = displayMainMenu(context, ussdSessionid, msisdn);
           
        }    
        MLogger.Log(this, LogLevel.DEBUG, msg);
        return msg;
    }
    
    public String processConfirmMenu(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        this.FreeFlow = "FB";
        StringBuilder sb = new StringBuilder();
        MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input "+input);
        RequestEnum reqEnum = bundleSession.getSelection();
        MLogger.Log(this, LogLevel.DEBUG, "RequestEnum "+reqEnum.toString());
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() == 12) && (StringUtils.isNumeric(input.trim())) ){
           DataINFOSERVICE dbInfosService = new DataINFOSERVICE();
            input =  input.trim();
            bundleSession.setSerialID(input);
            String customerMSISDN = bundleSession.getCustomerMSISDN();
            String serialID = bundleSession.getSerialID();
            MLogger.Log(this, LogLevel.DEBUG, "RequestEnum "+reqEnum.toString());
            if(input.equals(serialID) && reqEnum == RequestEnum.SERIAL){
                InfoServiceClient serialKeyCheck = dbInfosService.getSerialKey(serialID);
                if(serialKeyCheck == null){
                    msg = "Votre requete est en cours de traitement. Le numero sera recharge dans 24h.\n";
                    dbInfosService.insertTransacCarteMalGratter(ussdSessionid, msisdn, customerMSISDN, serialID, 0);
                }else{
                    msg = "Desole cette serie est deja utiliser. Merci de recharger votre compte.\n";
                }
            }else{
                msg = displayMainMenu(context, ussdSessionid, msisdn);
            } 
            String contextKey = GetContextKey(ussdSessionid);
            context.removeAttribute(contextKey);                     
        }else if(input.trim().length() != 14){
            msg = "Le numero de serie saisie est incorrect. Veuillez saisir le bon numero de serie.\n";
        }else{
            //naughty user, he entered wrong information
            msg = displayMainMenu(context, ussdSessionid, msisdn);
        }    
        MLogger.Log(this, LogLevel.DEBUG, msg);
        return msg;
    }
    
    private void processShortCut(String msisdn, String ussdSessionid, RequestEnum requestEnum, ServletContext context){
        BundleSession bundleSession = new BundleSession();
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"| Processing Shortcut request|RequestEnum = "+requestEnum.name());
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setStep(2);
        bundleSession.setSelection(requestEnum);
        String contextKey = GetContextKey(ussdSessionid);
        context.setAttribute(contextKey, bundleSession);
        
    }
    
    protected void TraceParametersAndHeaders(HttpServletRequest request){
        
        Enumeration<String> headerEnum = request.getHeaderNames();
        MLogger.Log(this, LogLevel.DEBUG, "Tracing HTTP Headers and Parameters ");
        
        while(headerEnum.hasMoreElements()) {
            String headerName = headerEnum.nextElement();
            String headerValue = request.getHeader(headerName);
            //System.out.println("Header:- "+headerName+": "+headerValue);
            MLogger.Log(this, LogLevel.DEBUG, "Header:- "+headerName+": "+headerValue);
        } 
        
        Map<String, String[]> map = request.getParameterMap();
        //Reading the Map
        //Works for GET && POST Method
        for(String paramName:map.keySet()) {
            String[] paramValues = map.get(paramName);

            //Get Values of Param Name
            for(String valueOfParam:paramValues) {
                //Output the Values
                //System.out.println("Value of Param with Name "+paramName+": "+valueOfParam);
                //ystem.out.println("Parameter:- "+paramName+": "+valueOfParam);
                MLogger.Log(this, LogLevel.DEBUG, "Parameter:- "+paramName+": "+valueOfParam);
            }
        }           
        
    }
    
    protected String GetContextKey(String ussdSessionid){
        if (ContextKeyName == null)
            throw new RuntimeException("ContextKeyName not set");
        return ContextKeyName+"-"+ussdSessionid;
    } 
    
    protected boolean IsMSISDNAllowed(int msisdn){
        if((supportedMSISDNs == null) || (supportedMSISDNs.size() == 0))
            return true;
        if(msisdn == 0)
            return true;
        for(int n : supportedMSISDNs){
            if(n==msisdn)
                return true;
        }
        
        return false;
    }
}
