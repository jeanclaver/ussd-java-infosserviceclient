/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClients;


import java.util.Date;
import java.util.concurrent.TimeUnit;
import mtngc.ussd.infoServicesClient.data.DataINFOSERVICE;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.UCIPUpdateBalanceAndDateResponse;
import ucipclient.UCIPUpdateServiceClassResponse;
import ucipclient.UCIPUpdateRefillIDResponse;


/**
 *
 * @author Administrateur
 */
public class ProvisioningEngine {
    
    private static int[] supportedServiceClasses = {29, 38, 102, 31, 23, 35, 36, 39 ,1, 6,15, 30, 19, 20, 66, 40};
     
    ResponseEnum execute(String msisdn, String transactionId, RequestEnum reqEnum, String externalData1){
        ResponseEnum respEnum  = ResponseEnum.SUCCESS;  
        
        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"| Fetching Service Class ");
        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        ucipclient.UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
        
        if(getAccountResponse != null){
            int sc = getAccountResponse.getServiceClass();
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Service Class="+sc);

            if(this.IsServiceClassAllowed(sc)){
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Funds are sufficient");
                if(reqEnum == RequestEnum.CODE_SERVICE){
                    UCIPUpdateServiceClassResponse ucipUpdate = ucipEngine.UpdateServiceClass(msisdn, transactionId, sc, 66);
                    if(ucipUpdate.getResponseCode()==0){
                        UCIPGetAccountDetailsResponse getAccountSc = ucipEngine.GetAccountDetails(msisdn, transactionId);
                        int updateSc = getAccountSc.getServiceClass();
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Service Class="+sc);
                    }
                    respEnum  = ResponseEnum.SUCCESS;
                }else if(reqEnum == RequestEnum.SERVICE_DIGITAL){
                    UCIPUpdateServiceClassResponse ucipUpdate = ucipEngine.UpdateServiceClass(msisdn, transactionId, 66, 20);
                    if(ucipUpdate.getResponseCode()==0){
                        //int updateSc = getAccountResponse.getServiceClass();
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Service Class="+sc);
                    }
                    respEnum  = ResponseEnum.SUCCESS;
                }
                
            }else {
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"| Not allowed");
                respEnum  = ResponseEnum.NOT_ALLOWED;
                // ici faire l'insertion dans la base de données Non autoriser...
            }
        }else {
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Failed to get account details");
                 
            respEnum  = ResponseEnum.ERROR; 
        }       
        
        return respEnum;    
    }
    
    private boolean IsServiceClassAllowed(int sc){
        for(int n : supportedServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
//    private boolean DaActivate(int da){
//        for(int n : daActivtate){
//            if(n==da)
//                return true;
//        }
//        
//        return false;
//    }
//     
}
