/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.infoServicesClients;


import java.util.Date;
import java.util.concurrent.TimeUnit;
import mtngc.ussd.infoServicesClient.data.DataINFOSERVICE;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.UCIPUpdateBalanceAndDateResponse;
import ucipclient.UCIPUpdateServiceClassResponse;
import ucipclient.UCIPUpdateRefillIDResponse;
import ucipclient.UCIPUpdateOfferResponse;


/**
 *
 * @author mpdiallo
 */
public class ProvisioningHorsForfaitsEngine {
    
    private static int[] supportedServiceClasses = {2,29, 38, 102, 31, 23, 35, 36, 39 ,1, 6,15, 30, 19, 20, 66, 40};
     
    ResponseEnum execute(String msisdn, String transactionId, RequestEnum reqEnum, String externalData1){
        ResponseEnum respEnum  = ResponseEnum.SUCCESS;  
//        String externalRefill = "PAYG";
//        String refill_Id = "80000";
        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"| Fetching Service Class ");
        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        ucipclient.UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
        //UCIPUpdateRefillProfilIDResponse refillProfileIdResponse = ucipEngine.RefillProfileID(msisdn, transactionId, refill_Id, externalRefill);
        if(getAccountResponse != null){
            int sc = getAccountResponse.getServiceClass();
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Service Class="+sc);

            if(this.IsServiceClassAllowed(sc)){
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Funds are sufficient");
                 MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Funds are sufficient");
                
                UCIPUpdateOfferResponse updateOfferResponse = null;
                if(reqEnum == RequestEnum.DEACTIVATE){
                    updateOfferResponse = ucipEngine.DeleteOffer(msisdn, transactionId, 80000,2,null);
                   
                }else{
                    updateOfferResponse = ucipEngine.UpdateOffer(msisdn, transactionId, 80000,2, null);
                }
                int x = updateOfferResponse.getResponseCode();
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|ResponseCode = "+x);
                if((x == 0) && (!updateOfferResponse.isError())){
                    respEnum = ResponseEnum.SUCCESS;
                            
                }else{ 
                    if ((reqEnum == RequestEnum.ACTIVATE) && (x==136)){
                        respEnum = ResponseEnum.ALREADEXIST;
                        
                    }else if ((reqEnum == RequestEnum.DEACTIVATE) && (x==165)){
                        respEnum = ResponseEnum.NOTEXIST;
                        
                    }else {
                    respEnum  = ResponseEnum.ERROR;
                    }
                }
                
            }else {
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"| Not allowed");
                respEnum  = ResponseEnum.NOT_ALLOWED;
                // ici faire l'insertion dans la base de données Non autoriser...
            }
        }else {
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|"+reqEnum.toString()+"|Failed to get account details");
                 
            respEnum  = ResponseEnum.ERROR; 
        }       
        
        return respEnum;    
    }
    
    private boolean IsServiceClassAllowed(int sc){
        for(int n : supportedServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
//    private boolean DaActivate(int da){
//        for(int n : daActivtate){
//            if(n==da)
//                return true;
//        }
//        
//        return false;
//    }
//     
}
